import React, { useState } from "react";
// import "./style/login.css";
import Swal from "sweetalert2";
import axios from "axios";
import { useHistory } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:8080/users/sign-in",
        {
          email: email,
          password: password,
        }
      );

      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login berhasil",
          showCancelButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.users.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.users.role);
        history.push("/home");
        window.location.reload();
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showCancelButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  return (
    <div className="bg-[url('https://cdn.wallpapersafari.com/21/14/TPLZrB.jpg')] bg-fixed bg-no-repeat bg-cover bg-white/10">
      <div className="flex justify-center items-center min-h-screen">
        <div className="w-96 flex flex-col px-3.5 border shadow-xl shadow-black bg-black/20 md:p-3 p-3 m-10 outline-none rounded-xl">
          <div>
            <header className="text-white text-4xl font-bold text-center mb-9">
              Login
            </header>
          </div>
          <form onSubmit={login}>
            <div className="mb-px">
              <div>
                <input
                  className=" border-0 rounded-3xl bg-white/10 pl-11 py-1.5 w-full text-white"
                  placeholder="Masukkan Email Anda..."
                  type="text"
                  value={email}
                  required
                  onChange={(e) => setEmail(e.target.value)}
                />
                <i className="bx bx-user"></i>
              </div>
              <div className="mt-6">
                <input
                  className=" border-0 rounded-3xl text-red-100 bg-white/10 pl-11 py-1.5 w-full"
                  placeholder="Masukkan Password Anda..."
                  type="password"
                  value={password}
                  required
                  onChange={(e) => setPassword(e.target.value)}
                />
                <i className="bx bx-lock-alt"></i>
              </div>
            </div>
            <div className="w-full mt-3  bg-white border rounded-3xl text-center p-2">
              <input type="submit" className="submit" value="Login" />
            </div>
            <div className="flex flex-row justify-between text-white text-base mt-2.5">
            </div>
            <p className="text-white">
              Jika belum punya akun silahkan
              <a style={{ textDecoration: "none" }} href="/register">
                Register
              </a>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
