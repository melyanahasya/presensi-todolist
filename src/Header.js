import React from 'react'

function Header() {
  return (
    <div className="font-bold text-center">
        <h1 className="text-white p-4 text-3xl">Todo List</h1>
    </div>
  )
}

export default Header