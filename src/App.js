import Register from "./Register";
import "./App.css";
import FormToDoList from "./component/FormToDoList";
import Login from "./Login";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PageHome from "./page/PageHome";
import Presensi from "./component/Presensi";
import Edit from "./component/Edit";
import EditMasuk from "./component/EditMasuk";
import Footer from "./component/Footer";
import Profil from "./page/Profil";

function App() {

  return (
    <div>
      <div>
        
        <BrowserRouter>
          <main>
            <Switch>
              <Route path="/" component={Login} exact />
              <Route path="/formToDoList" component={FormToDoList} exact />
              <Route path="/presensi" component={Presensi} exact />
              <Route path="/profil" component={Profil} exact />
              <Route path="/register" component={Register} exact />
              <Route path="/home" component={PageHome} exact />
              <Route path="/edit/:id" component={Edit} exact/>
              <Route path="/editMasuk/:id" component={EditMasuk} exact/>
            </Switch>
          </main>
        </BrowserRouter>
        {/* <Footer/> */}
      </div>
    </div>
  );
}

export default App;
