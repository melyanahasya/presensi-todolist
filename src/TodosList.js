import axios from "axios";
import React, { useState } from "react";
import Swal from "sweetalert2";
import { useHistory, useParams } from "react-router-dom";
import { Modal, Form, InputGroup } from "react-bootstrap";

export default function TodosList({ todos, setTodos, setEditTodo }) {
  const param = useParams();
  const [note, setNote] = useState("");
  const [show, setShow] = useState(false);
  const history = useHistory();
  const [noteId, setNoteId] = useState(0);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getById = async (id) => {
    await axios
      .get("http://localhost:8080/todolist/" + id)
      .then((res) => {
        setNote(res.data.note);
        setNoteId(res.data.id);
        console.log(res.data);
      })
      .catch((error) => {
        alert("Terjadi Keslahan" + error);
      });
  };

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Menghapus?",
      text: "Anda mengklik tombol!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus ini!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:8080/todolist/` + id);
      }
      window.location.reload();
    });
  };

  const Edit = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = {
      note: note,
    };

    try {
      await axios.put(`http://localhost:8080/todolist/${noteId}`, formData);
      Swal.fire({
        icon: "success",
        title: "Berhasil mengedit data",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/formToDoList");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // method untuk cheklist
  const chek = async (id) => {
    try {
      await axios.put(`http://localhost:8080/todolist/done/` + id);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans">
      <div className="border-0 bg-white/10 rounded-xl shadow-xl shadow-black p-6 m-3 w-full md:w-64 lg:w-3/4 lg:max-w-lg md:overflow-y-auto overflow-y-auto h-full">
        {todos.map((todo) => (
          <li className="flex mb-4 items-center " key={todo.id}>
            <div className="flex md:gap-2 gap-1 md:text-base text-xs">
              <p className="text-white">{todo.done}</p>
              <p
                className={`list ${
                  todo.completed ? "completed" : ""
                } md:w-56 w-20 text-grey-darkest md:overflow-x-auto overflow-x-auto text-white font-bold md:text-xl text-sm`}
              >
                {todo.note}
              </p>
            </div>

            <div className="flex md:gap-2 gap-1 ml-9 md:text-base text-xs">
              <button
                className="flex-no-shrink p-1 h-9 ml-4 mr-2 border-2 rounded hover:text-white text-green border-green hover:bg-green"
                onClick={() => chek(todo.id)}
              >
                <i className="fas fa-check"></i>
              </button>
              <button
                className="flex-no-shrink p-1 h-9 md:ml-3 ml-1 border-2 rounded text-red border-red hover:text-white hover:bg-red"
                onClick={() => {
                  setShow(true);
                  getById(todo.id);
                }}
              >
                <i className="fas fa-edit"></i>
              </button>
              <button
                className="flex-no-shrink p-1 h-9 md:ml-4 ml-2 border-2 rounded text-red border-red hover:text-white hover:bg-red"
                onClick={() => deleteUser(todo.id)}
              >
                <i className="fas fa-trash-alt"></i>
              </button>
            </div>

            <Modal show={show} onHide={handleClose}>
              <Modal.Header
                closeButton
                className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')] bg-no-repeat bg-cover"
              >
                <Modal.Title>Edit </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form onSubmit={Edit}>
                  <div className="mb-3">
                    <Form.Label>
                      <strong>Note</strong>
                    </Form.Label>
                    <InputGroup className="d-flex gap-3">
                      <Form.Control
                        onChange={(e) => setNote(e.target.value)}
                        required
                        placeholder="note"
                        value={note}
                      ></Form.Control>
                    </InputGroup>
                  </div>
                  <button className="mx-1 button-btl btn" onClick={handleClose}>
                    Close
                  </button>
                  <button
                    type="submit"
                    className="mx-1 button-btl btn"
                    onClick={handleClose}
                  >
                    save
                  </button>
                </Form>
              </Modal.Body>
            </Modal>
          </li>
        ))}
      </div>
    </div>
  );
}
