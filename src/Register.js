import React, { useState } from "react";
// import "./style/register.css";
import axios from "axios";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nama, setNama] = useState("");
  const [gambar, setGambar] = useState("");
  const [telepon, setTelepon] = useState("");
  const [alamat, setAlamat] = useState("");
  const history = useHistory();

  const register = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
    formData.append("nama", nama);
    formData.append("role", "USER");

    try {
      await axios.post("http://localhost:8080/users/sign-up", formData);

      Swal.fire({
        icon: "success",
        title: "Berhasil register",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div
      className="bg-[url('https://cdn.wallpapersafari.com/21/14/TPLZrB.jpg')] bg-fixed bg-no-repeat bg-cover bg-white/10"
      style={{ backgroundPosition: "55% 50%" }}
    >
      <div className="flex justify-center items-center min-h-screen mx-32">
        <div className="w-[700%] md:w-96 flex flex-col px-3.5 border shadow-xl shadow-black bg-black/20 p-6 m-10  outline-none rounded-xl">
          <div className="top">
            <header className="text-white font-bold text-center text-3xl">
              Register
            </header>
          </div>
          <form onSubmit={register}>
            <div className="mb-px">
              
              <div className="mt-[-6%]">
                <input
                  className="mt-5 border-0 rounded-3xl text-red-50 bg-white/10 pl-11 py-1.5 w-full"
                  placeholder="Masukkan Username Anda..."
                  type="text"
                  value={nama}
                  required
                  onChange={(e) => setNama(e.target.value)}
                />
                <i className="bx bx-lock-alt"></i>
              </div>
              <div>
                <input
                  className="mt-5 border-0 rounded-3xl text-white bg-white/10 pl-11 py-1.5 w-full"
                  placeholder="Masukkan Email Anda..."
                  type="text"
                  value={email}
                  required
                  onChange={(e) => setEmail(e.target.value)}
                />
                <i className="bx bx-user"></i>
              </div>
              <div className="mt-[-6%]">
                <input
                  className="mt-5 border-0 rounded-3xl text-red-50 bg-white/10 pl-11 py-1.5 w-full"
                  placeholder="Masukkan Password Anda..."
                  type="password"
                  value={password}
                  required
                  onChange={(e) => setPassword(e.target.value)}
                />
                <i className="bx bx-lock-alt"></i>
              </div>
            </div>
            <div className="w-full mt-3 bg-white border shadow-xl shadow-black rounded-3xl text-center">
              <button
                variant="primary"
                type="submit"
                className="mx-1 buton btn"
              >
                Register
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
