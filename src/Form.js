import axios from "axios";
import React, { useEffect, useState } from "react";

export default function Form({
  input,
  setInput,
  todos,
  setTodos,
  editTodo,
  setEditTodo,
}) {
  const updateTodo = (title, id, completed) => {
    const newTodo = todos.map((todo) =>
      todo.id === id ? { title, id, completed } : todo
    );
    setTodos(newTodo);
    setEditTodo("");
  };

  useEffect(() => {
    if (editTodo) {
      setInput(editTodo.title);
    } else {
      setInput("");
    }
  }, [setInput, editTodo]);

  const onInputChange = (event) => {
    setTodolist(event.target.value);
  };

  const [todolist, setTodolist] = useState("");

  const onFormSubmit = async (event) => {
    event.preventDefault();
    if (!editTodo) {
      try {
        await axios.post(
          `http://localhost:8080/todolist/post?note=${todolist}&userId=${localStorage.getItem(
            "userId"
          )}`
        );
        window.location.reload();
      } catch (error) {
        console.log(error);
      }
    } else {
      updateTodo(input, editTodo.id, editTodo.completed);
    }
  };
  return (
    <form onSubmit={onFormSubmit} className="md:ml-[36%] ml-6">
      <input
        className="flex-no-shrink p-2 ml-4 mr-2 border-2 rounded hover:text-black text-green border-green hover:bg-green"
        type="text"
        placeholder="Tambahkan Sesuatu..."
        required
        onChange={onInputChange}
      />
      <button
        className="flex-no-shrink p-2 border-2 rounded text-teal border-teal hover:text-white hover:bg-teal"
        type="submit"
      >
        {editTodo ? "OK" : "Add"}
      </button>
    </form>
  );
}
