import React from "react";
import { useHistory } from "react-router-dom";
import Footer from "../component/Footer";

export default function PageHome() {
  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/");
  };
  return (
    <div className="h-screen border border-red-800 bg-[url('https://c0.wallpaperflare.com/preview/280/903/583/cold-dawn-desktop-backgrounds-hd-wallpaper.jpg')] bg-fixed bg-no-repeat bg-cover">
      <div className="flex justify-center gap-7 p-3 border-0 bg-white/10 outline-none text-white text-xl">
        <div className="w-4/5 text-4xl">
          <a href="/home">
            <i className="fas fa-book-reader"> </i>
          </a>
        </div>

        <div>
          <a href="/profil">profil</a>
        </div>

        <a className="btn text-white text-xl" onClick={logout} href="/">
          <i className="fas fa-sign-out-alt"></i>
        </a>
      </div>

      <div className="block gap-1">
        <div className="md:mt-6 text-white/60 font-bold md:text-5xl text-3xl">
          <h1 className="text-center">Presensi & To-Do-List</h1>
        </div>
        <div className="flex md:gap-12 justify-center mx-3 -mt-12">
         

          <div className="mt-40 md:w-72 w-96 mr-5 md:mt-40 border shadow-xl shadow-black p-3 bg-white/10 hover:bg-white/20 rounded-xl">
            <a href="/presensi">
              <div>
                <img
                  className="md:w-64 w-32 rounded-xl opacity-90"
                  src="http://bkd.cilacapkab.go.id/packages/upload/category/images/lokasi0.jpg"
                  alt=""
                />
              </div>
              <div className="mt-3">
                <img
                  className="md:w-64 rounded-xl opacity-90"
                  src="https://id1.dpi.or.id/uploads/images/2022/05/image_750x395_628ca6f63fec1_1.jpg"
                  alt=""
                />
              </div>
            </a>
          </div>

          <div className="mt-40 md:mt-40 border shadow-xl shadow-black p-3 bg-white/10 hover:bg-white/20 rounded-xl ml-6">
            <a href="/formToDoList">
              <img
                className="md:w-72 w-96 rounded-xl opacity-90"
                src="https://img.freepik.com/free-vector/colorful-list-collection-with-flat-design_23-2147934852.jpg?w=2000"
                alt=""
              />
            </a>
          </div>
        </div>
      </div>
       <div className="mt-11">
       <Footer/>
       </div>
    </div>
  );
}
