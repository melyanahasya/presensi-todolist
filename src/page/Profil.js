import axios from "axios";
import React, { useEffect, useState } from "react";
import { InputGroup, Modal, Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Profil() {
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show, setShow] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [gambar, setGambar] = useState("");
  const [telepon, setTelepon] = useState("");

  const [profil, setProfil] = useState({
    email: "",
    password: "",
    nama: "",
    gambar: null,
    telepon: "",
    alamat: "",
  });
  const getProfil = async () => {
    await axios
      .get(`http://localhost:8080/users/${localStorage.getItem("userId")}`)
      .then((res) => {
        setProfil(res.data.data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getProfil();
  }, []);

  const Edit = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
    formData.append("nama", nama);
    formData.append("file", gambar);
    formData.append("telepon", telepon);
    formData.append("alamat", alamat);

    try {
      await axios.put(
        `http://localhost:8080/users/${localStorage.getItem("userId")}`,
        formData
      );

      Swal.fire({
        icon: "success",
        title: "Sukses Mengedit data",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get(`http://localhost:8080/users/${localStorage.getItem("userId")}`)
      .then((response) => {
        const page = response.data.data;
        setEmail(page.email);
        setPassword(page.password);
        setNama(page.nama);
        setAlamat(page.alamat);
        setGambar(page.gambar);
        setTelepon(page.telepon);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  return (
    <div className="mt-8">
      <main>
        <div className=" h-72  block h-500-px">
          <div className=" absolute top-0 w-full h-72 bg-center bg-cover bg-[url('https://cdn.wallpapersafari.com/21/14/TPLZrB.jpg')]">
            <span
              id="blackOverlay"
              className="w-full h-full absolute opacity-50 bg-black"
            ></span>
          </div>
          <div className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px">
            <svg
              className="absolute bottom-0 overflow-hidden"
              preserveAspectRatio="none"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-blueGray-200 fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </div>
        <div className="relative py-16 md:px-16 bg-blueGray-200">
          <div className="container mx-auto px-4 ">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
              <div className="px-6">
                <div className="flex flex-wrap justify-center">
                  <div className="md:w-full w-12  px-4 lg:order-2 flex justify-center">
                    <div className="w-full">
                      <img
                        alt="..."
                        src={profil.gambar}
                        className=" border-8 border-black/60 md:w-56 w-32 shadow-xl rounded-full h-auto align-middle  absolute -m-16 -ml-20 lg:-ml-16 max-w-xs"
                      />
                    </div>
                  </div>
                  {/* <div className="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
             
            </div> */}
                  {/* <div className="w-full lg:w-4/12 px-4 lg:order-1">
              <div className="flex justify-center py-4 lg:pt-4 pt-8">
                <div className="mr-4 p-3 text-center">
                  <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">22</span><span className="text-sm text-blueGray-400">Friends</span>
                </div>
                <div className="mr-4 p-3 text-center">
                  <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">10</span><span className="text-sm text-blueGray-400">Photos</span>
                </div>
                <div className="lg:mr-4 p-3 text-center">
                  <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">89</span><span className="text-sm text-blueGray-400">Comments</span>
                </div>
              </div>
            </div> */}
                </div>
                <div className="text-center mt-12" onSubmit={getProfil}>
                  <h3 className="md:text-4xl text-2xl font-semibold leading-normal mt-20 mb-2 text-blueGray-700 ">
                    {profil.nama}
                  </h3>
                  <div className="md:text-base text-xs leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                    <i className="fas fa-map-marker-alt mr-2 md:text-base text-blueGray-400"></i>
                    Alamat : {profil.alamat}
                  </div>
                  <div className="mb-2 text-blueGray-600 mt-10 md:text-base text-sm">
                    <i class="fas fa-envelope"></i> Email : {profil.email}
                  </div>
                  <div className="mb-2 text-blueGray-600  md:text-base text-sm">
                    <i class="fas fa-phone-volume"></i> No. telepon :
                     {profil.telepon}
                  </div>
                </div>
                <div className="mt-10 py-10 border-t border-blueGray-200 text-center">
                  <div className="flex flex-wrap justify-center">
                    <div className="w-full lg:w-9/12 px-4">
                      <p className="mb-4 md:text-lg text-base leading-relaxed text-blueGray-700">
                        {/* Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Soluta, cum ex. Perspiciatis cumque quas ullam
                        rerum molestiae, alias distinctio deserunt explicabo.
                        Optio aut provident soluta ipsum sapiente vel facilis
                        ad. */}

                        Kelola informasi profil anda untuk mengontrol, melindungi, dan mengamankan akun 
                      </p>
                      <div className="py-6 px-3 mt-16 md:mt-16 sm:mt-0">
                        <button
                          onClick={handleShow}
                          className="bg-pink-500 active:bg-pink-600 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1 ease-linear transition-all duration-150"
                          type="button"
                        >
                          Edit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer className="relative bg-blueGray-200 pt-8 pb-6 mt-8">
            <div className="container mx-auto px-4 ">
              <div className="flex flex-wrap items-center md:justify-between justify-center">
                <div className="w-full md:w-6/12 px-4 mx-auto text-center">
                  <div className="text-sm text-blueGray-400 font-semibold py-1">
                    {/* Made with <a href="https://www.creative-tim.com/product/notus-js" className="text-blueGray-500 hover:text-gray-800" target="_blank">Notus JS</a> by <a href="https://www.creative-tim.com" className="text-blueGray-500 hover:text-blueGray-800" target="_blank"> Creative Tim</a>. */}
                    Melyana hasya depratiwi.
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header
            closeButton
            className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')] bg-no-repeat bg-cover"
          >
            <Modal.Title>Edit </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={Edit}>
              <div className="mb-3">
                <Form.Label>
                  <strong>Nama</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="nama..."
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Alamat</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="alamat..."
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Gambar</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    type="file"
                    onChange={(e) => setGambar(e.target.files[0])}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Telepon</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="No telepon..."
                    type="number"
                    value={telepon}
                    onChange={(e) => setTelepon(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="email..."
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="password..."
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <button className="mx-1 button-btl btn" onClick={handleClose}>
                Close
              </button>
              <button
                type="submit"
                className="mx-1 button-btl btn"
                onClick={handleClose}
              >
                save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
      </main>
    </div>
  );
}
