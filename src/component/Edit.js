import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";

export default function Edit() {
  const [status, setStatus] = useState("");
  const history = useHistory();
  const param = useParams();

  useEffect(() => {
    axios
      .get("http://localhost:8080/Absensi_Pulang/" + param.id)
      .then((response) => {
        const getpresensi = response.data.data;
        setStatus(getpresensi.status);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);

  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put("http://localhost:8080/Absensi_Pulang/" + param.id, {
        status,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit data",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/presensi");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="mx-5 px-56">
      <div className="my-5">
        <Form onSubmit={Put}>
          <div className="name mb-3">
            <Form.Label>
              <strong> Status </strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="status..."
                value={status}
                onChange={(e) => setStatus(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button className="buton btn" type="submit">
              Save
            </button>
          </div>
        </Form>
      </div>
    </div>
  );
}
