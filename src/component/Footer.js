import React from "react";

export default function Footer() {
  return (
    <div>
      <footer className=" p-4 bg-gradient-to-r from-[#A0C3D2] to-[#E98EAD] sm:p-6">
        <div className="md:flex md:justify-between">
          <div className="mb-6 md:mb-0">
            <a href="https://flowbite.com/" className="flex items-center">
              <img
                src="https://flowbite.com/docs/images/logo.svg"
                className="h-8 mr-3"
                alt="FlowBite Logo"
              />
              <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-black">
                Melyana hasya
              </span>
            </a>
          </div>
          <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
            <div>
              <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Resources
              </h2>
              <ul className="text-gray-600 dark:text-black">
                <li>
                  <a
                    href="https://tailwindcss.com/"
                    className="hover:underline"
                    target={"_blank"}
                  >
                    Tailwind CSS
                  </a>
                </li>
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Follow us
              </h2>
              <ul className="text-black dark:text-gray-400">
                <li className="mb-4">
                  <a
                    href="https://gitlab.com/melyanahasya/presensi-todolist-be.git"
                    target={"_blank"}
                    className="hover:underline "
                  >
                    Gitlab
                  </a>
                </li>
              </ul>
            </div>
            <div>
              <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Legal
              </h2>
              <ul className="text-black dark:text-gray-400">
                <li className="mb-4">
                  <a href="#" className="hover:underline">
                    Privacy Policy
                  </a>
                </li>
                <li>
                  <a href="#" className="hover:underline">
                    Terms &amp; Conditions
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
        <div className="sm:flex sm:items-center sm:justify-between">
          <span className="text-sm text-black sm:text-center dark:text-gray-400">
            © 2023
            <a href="https://flowbite.com/" className="hover:underline">
              projek
            </a>
            . Melyana hasya d.
          </span>
          <div className="flex mt-4 text-xl space-x-6 sm:justify-center sm:mt-0">
            <a
              className="text-black hover:text-gray-900 dark:hover:text-white "
              href="https://www.facebook.com/"
              target={"_blank"}
            >
              <i className="fab fa-facebook"></i>
            </a>

            <a
              className="text-black hover:text-gray-900 dark:hover:text-white "
              href="https://www.instagram.com/"
              target={"_blank"}
            >
              <i className="fab fa-instagram"></i>
            </a>

            <a
              className="text-black hover:text-gray-900 dark:hover:text-white"
              href="https://twitter.com/"
              target={"_blank"}
            >
              <i className="fab fa-twitter"></i>
            </a>
            <a
              className="text-black hover:text-gray-900 dark:hover:text-white"
              href="https://gitlab.com/melyanahasya/presensi-todolist-be.git"
              target={"_blank"}
            >
              <i className="fab fa-gitlab"></i>
            </a>
          </div>
        </div>
      </footer>
    </div>
  );
}
