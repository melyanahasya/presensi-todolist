import axios from "axios";
import React, { useEffect, useState } from "react";

export default function AbsenMasuk() {
  const [presensi, setPresensi] = useState([]);

  const getAllMasuk = async () => {
    await axios
      .get(`http://localhost:8080/Absensi_Masuk?userId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setPresensi(res.data.data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAllMasuk();
  }, []);
  return (
    <div>
      <div className="overflow-hidden overflow-x-auto rounded-lg md:px-16 text-center md:float-left">
        <table className="min-w-full divide-y divide-gray-200 mt-4 text-sm">
          <thead className="bg-gray-100">
            <tr>
              <th className="whitespace-nowrap px-4 py-2 text-left  font-medium text-gray-600">
                No
              </th>
              <th className="whitespace-nowrap px-4 py-2 text-left  font-medium text-gray-600">
                tanggal
              </th>
              <th className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-600">
                Absen Masuk
              </th>
              <th className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-600">
                Status
              </th>
              <th className="whitespace-nowrap px-4 py-2 text-left font-medium text-gray-600">
                Action
              </th>
            </tr>
          </thead>

          <tbody className="ml-10">
            {presensi.map((presensi, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{presensi.tanggal}</td>
                  <td>{presensi.absen_masuk}</td>
                  <td>{presensi.status}</td>
                  <td className="action">
                    <a href={"/editMasuk/" + presensi.id}>
                      <button
                        variant="warning"
                        style={{ border: "none" }}
                        className="mx-1"
                      >
                        <i className="far fa-edit"></i>
                      </button>
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
