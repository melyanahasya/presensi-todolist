import axios from "axios";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Form from "../Form";
import Header from "../Header";
import TodosList from "../TodosList";

export default function Home() {
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([]);
  const [editTodo, setEditTodo] = useState(null);

  const getAll = async () => {
    await axios
      .get(`http://localhost:8080/todolist?useresId=${localStorage.getItem("userId")}`)
      .then((res) => {
        setTodos(res.data.data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const history = useHistory();

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    history.push("/");
  };
  return (
    <div className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')] bg-no-repeat bg-cover">
      <div className="flex justify-center gap-7 p-3 border-0 bg-white/10 outline-none text-white text-xl">
        <div className="w-4/5 text-4xl">
          <a href="/home">
            <i className="fas fa-book-reader"> </i>
          </a>
        </div>

        <a className="btn text-white text-xl" onClick={logout} href="/">
          <i className="fas fa-sign-out-alt"></i>
        </a>
      </div>
      <div className="min-h-screen rounded-xl shadow-black">
        <div>
          <Header />
        </div>
        <div>
          <Form
            input={input}
            setInput={setInput}
            todos={todos}
            setTodos={setTodos}
            editTodo={editTodo}
            setEditTodo={setEditTodo}
          />
        </div>
        <div className="md:overflow-y-auto overflow-y-auto h-60 mt-7">
          <TodosList
            todos={todos}
            setTodos={setTodos}
            setEditTodo={setEditTodo}
          />
        </div>
      </div>
    </div>
  );
}
