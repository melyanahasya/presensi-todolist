import Swal from "sweetalert2";
import axios from "axios";
import { useState } from "react";
import AbsenMasuk from "./AbsenMasuk";
import AbsenPulang from "./AbsenPulang";
import { InputGroup, Modal, Form } from "react-bootstrap";

export default function Presensi() {
  const [show, setShow] = useState(false);
  const [addmasuk, setAddmasuk] = useState(false);

  const [status, setStatus] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const absenMasuk = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:8080/Absensi_Masuk", {
        status: status,
        userId: localStorage.getItem("userId"),
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Absen Masuk Sukses",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const addPulang = async (e) => {
    e.preventDefault();

    try {
      await axios.post("http://localhost:8080/Absensi_Pulang", {
        status: status,
        userId: localStorage.getItem("userId"),
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Absen Masuk Sukses",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')] min-h-screen bg-no-repeat text-white bg-cover px-4">
        <div className="flex justify-center gap-11 p-3 border-0 bg-white/10 outline-none text-white text-xl">
          <div className="w-4/5 text-4xl">
            <a href="/home">
              <i className="fas fa-book-reader"> </i>
            </a>
          </div>
          <h1 className="text-center mr-[35%] flex text-4xl font-bold">
            Presensi
          </h1>

          <div>
          <a href="/profil">profil</a>
        </div>

          <a href="/">
            <i className="fas fa-sign-out-alt"></i>
          </a>
        </div>

        <div className="flex gap-4 justify-center mt-2">
          <button
            onClick={() => setAddmasuk(true)}
            className="border p-2 rounded-lg"
            type="submit"
          >
            Absen masuk
          </button>
          <button onClick={handleShow} className="border p-2 rounded-lg">
            Absen Pulang
          </button>
        </div>

        <div className="block md:ml-8">
          <AbsenMasuk />
          <AbsenPulang />
        </div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header
            closeButton
            className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')]  bg-no-repeat bg-cover"
          >
            <Modal.Title>Add Pulang </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={addPulang}>
              <div className="mb-3">
                <Form.Label>
                  <strong>Status</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    onChange={(e) => setStatus(e.target.value)}
                    required
                    placeholder="status"
                    value={status}
                  />
                </InputGroup>
              </div>
              <button className="mx-1 button-btl btn" onClick={handleClose}>
                Close
              </button>
              <button
                type="submit"
                className="mx-1 button-btl btn"
                onClick={handleClose}
              >
                save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
        <Modal show={addmasuk} onHide={!addmasuk}>
          <Modal.Header
            closeButton
            className="bg-[url('https://wallpaperaccess.com/full/4441376.jpg')]  bg-no-repeat bg-cover"
          >
            <Modal.Title>Add Masuk</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={absenMasuk}>
              <div className="mb-3">
                <Form.Label>
                  <strong>Status</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    onChange={(e) => setStatus(e.target.value)}
                    required
                    placeholder="status"
                    value={status}
                  />
                </InputGroup>
              </div>
              <button
                className="mx-1 button-btl btn"
                onClick={() => setAddmasuk(false)}
              >
                Close
              </button>
              <button
                type="submit"
                className="mx-1 button-btl btn"
                onClick={() => setAddmasuk(false)}
              >
                save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}
